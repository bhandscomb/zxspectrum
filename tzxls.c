/* tzxls.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void block10 (FILE *f)
{
  unsigned char u, hdr[0x13];
  int p, l, p1, p2;
  // pause
  fread (&u, 1, 1, f); p = (int) u;
  fread (&u, 1, 1, f); p |= ((int) u) << 8;
  // length
  fread (&u, 1, 1, f); l = (int) u;
  fread (&u, 1, 1, f); l |= ((int) u) << 8;
  printf ("10: Std %04x", l);
  if (l != 0x13)
  {
    fseek (f, l, SEEK_CUR);
    putchar ('\n');
    return;
  }
  // might be a header?
  fread (&hdr, 0x13, 1, f);
  if ((hdr[0] != 0x00) || (hdr[1] > 3))
  {
    // nope...
    putchar ('\n');
    return;
  }
  l = (int) hdr[12];
  l |= ((int) hdr[13]) << 8;
  p1 = (int) hdr[14];
  p1 |= ((int) hdr[15]) << 8;
  p2 = (int) hdr[16];
  p2 |= ((int) hdr[17]) << 8;
  switch (hdr[1])
  {
    case 0:
      printf (" Program: \"%.10s\"", &hdr[2]);
      if (p1 < 32768) printf (" LINE %d", p1);
      putchar ('\n');
      break;
    case 1:
      printf (" Num array: \"%.10s\"\n", &hdr[2]);
      return;
    case 2:
      printf (" Char array: \"%.10s\"\n", &hdr[2]);
      return;
    case 3:
      printf (" Bytes: \"%.10s\" %d,%d\n", &hdr[2], p1, l);
      return;
  }
}

void block30 (FILE *f)
{
  unsigned char u;
  char s[256];
  fread (&u, 1, 1, f);
  printf ("30: Desc: \"");
  while (u > 0)
  {
    char c;
    fread (&c, 1, 1, f);
    putchar (c);
    u--;
  }
  printf ("\"\n");
}

void block32 (FILE *f)
{
  unsigned char u;
  int l;
  fread (&u, 1, 1, f); l = (int) u;
  fread (&u, 1, 1, f); l |= ((int) u) << 8;
  fseek (f, l, SEEK_CUR);
  printf ("32: ArcInfo %04x\n", l);
}

int main (int argc, char *argv[])
{
  FILE *f;
  char hdr[10];
  if (argc != 2)
  {
    puts ("Usage: tzxls <filename>");
    return 0;
  }
  f = fopen (argv[1], "rb");
  if (f == NULL)
  {
    puts ("Unable to open file");
    return 0;
  }
  fread (hdr, 10, 1, f);
  if (strncmp (hdr, "ZXTape!", 7) != 0)
  {
    puts ("Bad header");
    fclose (f);
    return 0;
  }
  printf ("TZX v%d.%02d\n", hdr[8], hdr[9]);
  for(;;)
  {
    printf ("%06x: ", ftell(f));
    fread (hdr, 1, 1, f);
    if (feof (f)) break;
    switch (hdr[0])
    {
      case 0x10: block10(f); continue;
      case 0x30: block30(f); continue;
      case 0x32: block32(f); continue;
      default: printf ("ID %02x = ???\n", hdr[0]); break;
    }
    break;
  }
  putchar ('\n');
  fclose (f);
  return 0;
}

