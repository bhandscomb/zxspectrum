/* tapls.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void block (FILE *f, unsigned char u1)
{
  unsigned char u, hdr[0x13];
  int l, p1, p2;
  // length (first byte read outside of this function)
  l = (int) u1;
  fread (&u, 1, 1, f); l |= ((int) u) << 8;
  printf ("Block %04x", l);
  if (l != 0x13)
  {
    fseek (f, l, SEEK_CUR);
    putchar ('\n');
    return;
  }
  // might be a header?
  fread (&hdr, 0x13, 1, f);
  if ((hdr[0] != 0x00) || (hdr[1] > 3))
  {
    // nope...
    putchar ('\n');
    return;
  }
  l = (int) hdr[12];
  l |= ((int) hdr[13]) << 8;
  p1 = (int) hdr[14];
  p1 |= ((int) hdr[15]) << 8;
  p2 = (int) hdr[16];
  p2 |= ((int) hdr[17]) << 8;
  switch (hdr[1])
  {
    case 0:
      printf (" Program: \"%.10s\"", &hdr[2]);
      if (p1 < 32768) printf (" LINE %d", p1);
      putchar ('\n');
      break;
    case 1:
      printf (" Num array: \"%.10s\"\n", &hdr[2]);
      return;
    case 2:
      printf (" Char array: \"%.10s\"\n", &hdr[2]);
      return;
    case 3:
      printf (" Bytes: \"%.10s\" %d,%d\n", &hdr[2], p1, l);
      return;
  }
}

int main (int argc, char *argv[])
{
  FILE *f;
  unsigned char u1;
  if (argc != 2)
  {
    puts ("Usage: tapls <filename>");
    return 0;
  }
  f = fopen (argv[1], "rb");
  if (f == NULL)
  {
    puts ("Unable to open file");
    return 0;
  }
  for(;;)
  {
    printf ("%06x: ", ftell(f));
    fread (&u1, 1, 1, f);
    if (feof (f)) break;
    block (f, u1);
  }
  putchar ('\n');
  fclose (f);
  return 0;
}

