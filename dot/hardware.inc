; hardware

next_reg_select		equ	$243b
next_reg_access		equ	$253b

turbo_mask		equ	3
turbo_max		equ	3

nxr_turbo		equ	$07

keyb_CAPS_V		equ	$fefe
keyb_A_G		equ	$fdfe
keyb_Q_T		equ	$fbfe
keyb_1_5		equ	$f7fe
keyb_0_6		equ	$effe
keyb_P_Y		equ	$dffe
keyb_ENTER_H		equ	$bffe
keyb_SPACE_B		equ	$7ffe
